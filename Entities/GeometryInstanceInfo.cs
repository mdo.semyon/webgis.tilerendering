﻿using Microsoft.SqlServer.Types;
using System.Collections.Generic;
 
namespace WebGIS.TileRendering
{
    public class ss_GeometryInstanceInfo<T>
    {
        /// <summary>
        /// Тип геометрии
        /// </summary>
        public OpenGisGeometryType ShapeType { get; set; }
        // Наборы точек геометрических фигур (линии, внешние и внутренние кольца полигонов)
        public List<List<ss_GeometryPointSequence<T>>> Points { get; set; }
        public ss_GeometryInstanceInfo<T>[] GeometryInstanceInfoCollection { get; set; }

    }
}
