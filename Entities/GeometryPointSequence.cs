﻿namespace WebGIS.TileRendering
{ 
    public class ss_GeometryPointSequence<T>
    {
        public T[] PointList { get; set; }
        public bool InnerRing { get; set; }
    }
}
