﻿using System.Drawing;
using WebGIS.TileRendering.Conversions;
 
namespace WebGIS.TileRendering
{
    public static class Icon2TileRendering
    {
        public static void DrawIconOnTile(Graphics graphics, 
            Bitmap iconImage, 
            int zoom, 
            double Lon, 
            double Lat, 
            int labelPosition,
            int tileX, 
            int tileY) 
        {
            int width = iconImage.Width;
            int height = iconImage.Height;

            _copyRegionIntoImage(
                graphics,
                iconImage, 
                new Rectangle(0, 0, width, height), 
                _getTargetBound(zoom, Lon, Lat, tileX, tileY, width, height, labelPosition));
        }

        #region private
        static void _copyRegionIntoImage(Graphics graphics, Bitmap srcBitmap, Rectangle srcRegion, Rectangle destRegion) 
        {
            graphics.DrawImage(srcBitmap, destRegion, srcRegion, GraphicsUnit.Pixel);
        }

        static Rectangle _getTargetBound(int zoom, double Lon, double Lat, int tileX, int tileY, int width, int height, int labelPosition)
        {
            int xPix = Helpers.FromLongitudeToXPixel(Lon, zoom);
            int yPix = Helpers.FromLatitudeToYPixel(Lat, zoom);

            Point positionOffset = _getPositionOffset(width, height, labelPosition);

            //координаты в пикселях относительно начала тайла
            int xPos = xPix - tileX * 256 - positionOffset.X - 1;
            int yPos = yPix - tileY * 256 - positionOffset.Y + 1;

            return new Rectangle(xPos, yPos, width, height);
        }

        /// <summary>
        /// Расчитывает смещение картинки относительно центра точечного объекта
        /// </summary>
        /// <param name="width">Ширина картинки</param>
        /// <param name="height">Высота картинки</param>
        /// <param name="stateID">Идентификатор состояния</param>
        /// <returns></returns>
        static Point _getPositionOffset(int width, int height, int labelPositionID)
        {
            Point fs = new Point();

            switch (labelPositionID)
            {
                case 0://Center
                    fs.X = (int)(width / 2);
                    fs.Y = (int)(height / 2);
                    break;
                case 1://Left
                    fs.X = width;
                    fs.Y = (int)(height / 2);
                    break;
                case 2://Right
                    fs.X = 0;
                    fs.Y = (int)(height / 2);
                    break;
                case 3://Top
                    fs.X = (int)(width / 2);
                    fs.Y = height;
                    break;
                case 4://Bottom
                    fs.X = (int)(width / 2);
                    fs.Y = 0;
                    break;
            }

            return fs;
        }
        #endregion
    }
} 