﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Types;
using System.Drawing;
 
namespace WebGIS.TileRendering
{
    public static class GeometryExtension
    {
        
        public static SqlGeometry ToLineString(this SqlGeometry poly)
        {
            var geomBuilder = new SqlGeometryBuilder();
            geomBuilder.SetSrid((0));
            geomBuilder.BeginGeometry(OpenGisGeometryType.LineString);
            var startpoint = poly.STStartPoint();
            geomBuilder.BeginFigure((double)startpoint.STX, (double)startpoint.STY);
            for (int i = 1; i <= poly.STNumPoints(); i++)
            {
                geomBuilder.AddLine((double)poly.STPointN(i).STX, (double)poly.STPointN(i).STY);
            }
            geomBuilder.EndFigure();
            geomBuilder.EndGeometry();
            return geomBuilder.ConstructedGeometry;
        }

        public static OpenGisGeometryType GetGeometryType(this SqlGeometry geom)
        {
            return (OpenGisGeometryType)Enum.Parse(typeof(OpenGisGeometryType), (string)geom.STGeometryType());

        }
        
        // Возвращает последовательность точек первой геометрии
        public static List<List<ss_GeometryPointSequence<T>>> ss_ToGeometryPointsOfPolygon<T>(this SqlGeometry geom)
        {
            List<List<ss_GeometryPointSequence<T>>> pointsList = new List<List<ss_GeometryPointSequence<T>>>();
            pointsList.Add(ss_GetPolygonPointSequence<T>(geom));
            return pointsList;
        }

        public static List<List<ss_GeometryPointSequence<T>>> ss_ToGeometryPointsOfMultiPolygon<T>(this SqlGeometry geom)
        {
            List<List<ss_GeometryPointSequence<T>>> pointsList = new List<List<ss_GeometryPointSequence<T>>>();
            int numGeometries = (int)geom.STNumGeometries();
            List<ss_GeometryPointSequence<T>> points = new List<ss_GeometryPointSequence<T>>();

            for (int i = 1; i <= numGeometries; i++)
                pointsList.Add(ss_GetPolygonPointSequence<T>(geom.STGeometryN(i)));
            return pointsList;
        }

        public static List<List<ss_GeometryPointSequence<T>>> ss_ToGeometryPointsOfMultiPointLineString<T>(this SqlGeometry geom)
        {
            List<List<ss_GeometryPointSequence<T>>> pointsList = new List<List<ss_GeometryPointSequence<T>>>();
            int numGeometries = (int)geom.STNumGeometries();
            List<ss_GeometryPointSequence<T>> points = new List<ss_GeometryPointSequence<T>>();

            for (int i = 1; i <= numGeometries; i++)
            {
                points.Clear();
                points.Add(new ss_GeometryPointSequence<T>() { PointList = geom.STGeometryN(i).ss_ToPointsArray<T>(), InnerRing = false });
                pointsList.Add(points);
            }
            return pointsList;
        }

        static List<ss_GeometryPointSequence<T>> ss_GetPolygonPointSequence<T>(SqlGeometry geom)
        {
            List<ss_GeometryPointSequence<T>> pointsList = new List<ss_GeometryPointSequence<T>>();
            int numInteriorRing = (int)geom.STNumInteriorRing();

            ss_GeometryPointSequence<T> points = new ss_GeometryPointSequence<T>();
            points.PointList = geom.STExteriorRing().ss_ToPointsArray<T>();
            points.InnerRing = false;
            pointsList.Add(points);
            if (numInteriorRing > 0)
            {
                for (int i = 1; i <= numInteriorRing; i++)
                {
                    ss_GeometryPointSequence<T> innerPoints = new ss_GeometryPointSequence<T>();
                    innerPoints.PointList = geom.STInteriorRingN(i).ss_ToPointsArray<T>();
                    innerPoints.InnerRing = true;
                    pointsList.Add(innerPoints);
                }
            }
            return pointsList;
        }

        public static T[] ss_ToPointsArray<T>(this SqlGeometry geom)
        {
            List<T> result = new List<T>();
            
            if (typeof(T) == typeof(Point))
            {
                SqlGeometry fill;
                if (geom.STNumGeometries() > 1) fill = geom.STGeometryN(1);
                else fill = geom;
                int n = (int)fill.STNumPoints();

                object[] points = new object[n];
                for (int i = 1; i <= n; i++)
                {
                    points[i - 1] = new Point((int)fill.STPointN(i).STX, (int)fill.STPointN(i).STY);
                }

                foreach (var p in points)
                    result.Add((T)p);
            }
            else
            {
                SqlGeometry fill;
                if (geom.STNumGeometries() > 1) fill = geom.STGeometryN(1);
                else fill = geom;
                int n = (int)fill.STNumPoints();

                object[] points = new object[n];
                for (int i = 1; i <= n; i++)
                {
                    points[i - 1] = new PointF((float)fill.STPointN(i).STX, (float)fill.STPointN(i).STY);
                }

                foreach (var p in points)
                    result.Add((T)p);
            }

            return result.ToArray();
        }
    }
}
