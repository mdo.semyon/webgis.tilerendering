﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Types;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using WebGIS.TileRendering.Conversions;
 
namespace WebGIS.TileRendering 
{
    public static class ShapeToTileRendering
    {
        public static SqlGeometry ConvertToPixels(SqlGeometry geom, int zoom)
        {
            var pixelData = _lonLatToPixels(geom, zoom);
            var result = _createGeometry(pixelData);

            return result;
        }
        public static SqlGeometry Cut(SqlGeometry geom, int leftX, int rightX, int topY, int bottomY)
        {
            if (geom == null)
                return null;

            SqlGeometry result = null;
            SqlGeometry tile = _getTilePixelBound(leftX, rightX, topY, bottomY, 1);

            if (geom.STIntersects(tile))
                result = geom.STIntersection(tile);

            return result;
        }
        public static List<SqlGeometry> CutWithStroke(SqlGeometry geom, int leftX, int rightX, int topY, int bottomY)
        {
            List<SqlGeometry> result = new List<SqlGeometry>();
            SqlGeometry stroke = null;
            SqlGeometry contour;
            SqlGeometry tileLineString;
            SqlGeometry tobecut;
            SqlGeometry tile = _getTilePixelBound(leftX, rightX, topY, bottomY, 1);

            if (geom.STIntersects(tile))
            {
                var tiled = geom.STIntersection(tile);
                result.Add(tiled);
                switch (tiled.GetGeometryType())
                {
                    case OpenGisGeometryType.Polygon:
                        // Получаем контур полигона и внутренние кольца в виде MULTILINESTRING
                        contour = _polygonToMultiLineString(tiled);
                        // удаляем линии среза геометрии по границе тайла
                        tileLineString = tile.ToLineString();
                        tobecut = contour.STIntersection(tileLineString);
                        stroke = contour.STDifference(tobecut);

                        result.Add(stroke);
                        break;
                    case OpenGisGeometryType.MultiPolygon:
                        int geomnum = (int)tiled.STNumGeometries();
                        tileLineString = tile.ToLineString();
                        for (int g = 1; g <= geomnum; g++)
                        {
                            // Получаем контур полигона и внутренние кольца в виде MULTILINESTRING
                            contour = _polygonToMultiLineString(tiled.STGeometryN(g));
                            // удаляем линии среза геометрии по границе тайла
                            tobecut = contour.STIntersection(tileLineString);
                            stroke = contour.STDifference(tobecut);

                            result.Add(stroke);
                        }
                        break;
                    case OpenGisGeometryType.GeometryCollection:
                        int geomnum_ = (int)tiled.STNumGeometries();
                        tileLineString = tile.ToLineString();
                        for (int g = 1; g <= geomnum_; g++)
                        {
                            var g_obj = tiled.STGeometryN(g);
                            if (g_obj.GetGeometryType() == OpenGisGeometryType.Polygon)
                            {
                                // Получаем контур полигона и внутренние кольца в виде MULTILINESTRING
                                contour = _polygonToMultiLineString(g_obj);
                                // удаляем линии среза геометрии по границе тайла
                                tobecut = contour.STIntersection(tileLineString);
                                stroke = contour.STDifference(tobecut);

                                result.Add(stroke);
                            }

                        }
                        break;
                }
            }
            return result;
        }
        public static void ShiftAndDraw(Graphics graphics, SqlGeometry shape, Color fillcolor, int tileX, int tileY)
        {
            if (shape == null)
                return;

            SqlGeometry ring;
            int intnum;

            Point[] exteriorPoints = null;
            Point[] interiorPoints = null;
            switch (shape.GetGeometryType())
            {
                case OpenGisGeometryType.Polygon:

                    intnum = shape.STNumInteriorRing().Value;
                    ring = shape.STExteriorRing();

                    using (var gp = new GraphicsPath())
                    {
                        // 1. рисуем полигон без внутренних колец
                        if (ring != null && !ring.IsNull)
                        {
                            exteriorPoints = ring.ss_ToPointsArray<Point>();
                            for (int p = 0; p < exteriorPoints.Length; p++)
                            {
                                exteriorPoints[p].X = exteriorPoints[p].X - tileX * 256;
                                exteriorPoints[p].Y = exteriorPoints[p].Y - tileY * 256;
                            }
                            //_fillPolygonOnTile(graphics, fillcolor, exteriorPoints);
                            gp.AddPolygon(exteriorPoints);
                        }
                        // 2. рисуем внутренние кольца
                        for (int i = 1; i <= intnum; i++)
                        {
                            if (shape.STInteriorRingN(i) != null && !shape.STInteriorRingN(i).IsNull)
                            {
                                interiorPoints = shape.STInteriorRingN(i).ss_ToPointsArray<Point>();
                                for (int p = 0; p < interiorPoints.Length; p++)
                                {
                                    interiorPoints[p].X = interiorPoints[p].X - tileX * 256;
                                    interiorPoints[p].Y = interiorPoints[p].Y - tileY * 256;
                                }
                                //_fillTransparentPolygonOnTile(graphics, interiorPoints);
                                gp.AddPolygon(interiorPoints);
                            }
                        }
                        using (Brush brush = new SolidBrush(fillcolor))
                            graphics.FillPath(brush, gp);
                    }
                    

                    //// 1. рисуем полигон без внутренних колец
                    //if (ring != null && !ring.IsNull)
                    //{
                    //    exteriorPoints = ring.ss_ToPointsArray<Point>();
                    //    for (int p = 0; p < exteriorPoints.Length; p++)
                    //    {
                    //        exteriorPoints[p].X = exteriorPoints[p].X - tileX * 256;
                    //        exteriorPoints[p].Y = exteriorPoints[p].Y - tileY * 256;
                    //    }
                    //    _fillPolygonOnTile(graphics, fillcolor, exteriorPoints);
                    //}

                    //// 2. рисуем внутренние кольца
                    //for (int i = 1; i <= intnum; i++)
                    //{
                    //    if (shape.STInteriorRingN(i) != null && !shape.STInteriorRingN(i).IsNull)
                    //    {
                    //        interiorPoints = shape.STInteriorRingN(i).ss_ToPointsArray<Point>();
                    //        for (int p = 0; p < interiorPoints.Length; p++)
                    //        {
                    //            interiorPoints[p].X = interiorPoints[p].X - tileX * 256;
                    //            interiorPoints[p].Y = interiorPoints[p].Y - tileY * 256;
                    //        }
                    //        _fillTransparentPolygonOnTile(graphics, interiorPoints);
                    //    }
                    //}
                    break;
                case OpenGisGeometryType.MultiPolygon:
                    int subGeomNum = shape.STNumGeometries().Value;

                    for (int g = 1; g <= subGeomNum; g++)
                    {
                        var subGeom = shape.STGeometryN(g);
                        intnum = subGeom.STNumInteriorRing().Value;
                        ring = subGeom.STExteriorRing();

                        using (var gp = new GraphicsPath())
                        {
                            // 1. рисуем полигон без внутренних колец
                            if (ring != null && !ring.IsNull)
                            {
                                exteriorPoints = ring.ss_ToPointsArray<Point>();
                                for (int p = 0; p < exteriorPoints.Length; p++)
                                {
                                    exteriorPoints[p].X = exteriorPoints[p].X - tileX * 256;
                                    exteriorPoints[p].Y = exteriorPoints[p].Y - tileY * 256;
                                }
                                //_fillPolygonOnTile(graphics, fillcolor, exteriorPoints);
                                gp.AddPolygon(exteriorPoints);
                            }
                            // 2. рисуем внутренние кольца
                            for (int i = 1; i <= intnum; i++)
                            {
                                if (shape.STInteriorRingN(i) != null && !shape.STInteriorRingN(i).IsNull)
                                {
                                    interiorPoints = shape.STInteriorRingN(i).ss_ToPointsArray<Point>();
                                    for (int p = 0; p < interiorPoints.Length; p++)
                                    {
                                        interiorPoints[p].X = interiorPoints[p].X - tileX * 256;
                                        interiorPoints[p].Y = interiorPoints[p].Y - tileY * 256;
                                    }
                                    //_fillTransparentPolygonOnTile(graphics, interiorPoints);
                                    gp.AddPolygon(interiorPoints);
                                }
                            }
                            using (Brush brush = new SolidBrush(fillcolor))
                                graphics.FillPath(brush, gp);
                        }

                        //// 1. рисуем полигон без внутренних колец
                        //if (ring != null && !ring.IsNull)
                        //{
                        //    exteriorPoints = ring.ss_ToPointsArray<Point>();
                        //    for (int p = 0; p < exteriorPoints.Length; p++)
                        //    {
                        //        exteriorPoints[p].X = exteriorPoints[p].X - tileX * 256;
                        //        exteriorPoints[p].Y = exteriorPoints[p].Y - tileY * 256;
                        //    }
                        //    _fillPolygonOnTile(graphics, fillcolor, exteriorPoints);
                        //}
                        //// 2. рисуем внутренние кольца
                        //for (int i = 1; i <= intnum; i++)
                        //{
                        //    if (shape.STInteriorRingN(i) != null && !shape.STInteriorRingN(i).IsNull)
                        //    {
                        //        interiorPoints = shape.STInteriorRingN(i).ss_ToPointsArray<Point>();
                        //        for (int p = 0; p < interiorPoints.Length; p++)
                        //        {
                        //            interiorPoints[p].X = interiorPoints[p].X - tileX * 256;
                        //            interiorPoints[p].Y = interiorPoints[p].Y - tileY * 256;
                        //        }
                        //        _fillTransparentPolygonOnTile(graphics, interiorPoints);
                        //    }
                        //}
                    }
                    break;
                case OpenGisGeometryType.GeometryCollection:
                    subGeomNum = shape.STNumGeometries().Value;

                    for (int g = 1; g <= subGeomNum; g++)
                    {
                        ShiftAndDraw(graphics, shape.STGeometryN(g), fillcolor, tileX, tileY);
                    }
                    break;
            }
        }
        public static void ShiftAndDraw(Graphics graphics, 
            List<SqlGeometry> geoms, 
            Color fillcolor, 
            Color strokecolor, 
            Color centercolor, 
            int fillWidth, 
            int strokeWidth,
            int centerWidth,
            int tileX, 
            int tileY)
        {
            foreach (var geom in geoms)
            {
                _shiftAndDraw(graphics, geom, fillcolor, strokecolor, centercolor, fillWidth, strokeWidth, centerWidth, tileX, tileY);
            }
        }

        public static void _shiftAndDraw(Graphics graphics, SqlGeometry shape, Color fillcolor, Color strokecolor, Color centercolor, int fillWidth, int strokeWidth, int centerWidth, int tileX, int tileY)
        {
            if (shape == null)
                return;

            SqlGeometry ring;
            int intnum;

            Point[] exteriorPoints = null;
            Point[] interiorPoints = null;
            switch (shape.GetGeometryType())
            {
                case OpenGisGeometryType.LineString:
                case OpenGisGeometryType.MultiLineString:
                    _drawMultiLineStringBordered(graphics, shape, fillcolor, strokecolor, centercolor, fillWidth, strokeWidth, centerWidth, tileX, tileY);
                    break;
                case OpenGisGeometryType.Polygon:

                    intnum = shape.STNumInteriorRing().Value;
                    ring = shape.STExteriorRing();
                    using (var gp = new GraphicsPath())
                    {
                        // 1. рисуем полигон без внутренних колец
                        if (ring != null && !ring.IsNull)
                        {
                            exteriorPoints = ring.ss_ToPointsArray<Point>();
                            for (int p = 0; p < exteriorPoints.Length; p++)
                            {
                                exteriorPoints[p].X = exteriorPoints[p].X - tileX * 256;
                                exteriorPoints[p].Y = exteriorPoints[p].Y - tileY * 256;
                            }
                            //_fillPolygonOnTile(graphics, fillcolor, exteriorPoints);
                            gp.AddPolygon(exteriorPoints);
                        }

                        // 2. рисуем внутренние кольца
                        for (int i = 1; i <= intnum; i++)
                        {
                            if (shape.STInteriorRingN(i) != null && !shape.STInteriorRingN(i).IsNull)
                            {
                                interiorPoints = shape.STInteriorRingN(i).ss_ToPointsArray<Point>();
                                for (int p = 0; p < interiorPoints.Length; p++)
                                {
                                    interiorPoints[p].X = interiorPoints[p].X - tileX * 256;
                                    interiorPoints[p].Y = interiorPoints[p].Y - tileY * 256;
                                }
                                //_fillTransparentPolygonOnTile(graphics, interiorPoints);
                                gp.AddPolygon(interiorPoints);
                            }
                        }
                        using (Brush brush = new SolidBrush(fillcolor))
                            graphics.FillPath(brush, gp);
                    }

                    //// 1. рисуем полигон без внутренних колец
                    //if (ring != null && !ring.IsNull)
                    //{
                    //    exteriorPoints = ring.ss_ToPointsArray<Point>();
                    //    for (int p = 0; p < exteriorPoints.Length; p++)
                    //    {
                    //        exteriorPoints[p].X = exteriorPoints[p].X - tileX * 256;
                    //        exteriorPoints[p].Y = exteriorPoints[p].Y - tileY * 256;
                    //    }
                    //    _fillPolygonOnTile(graphics, fillcolor, exteriorPoints);
                    //}

                    //// 2. рисуем внутренние кольца
                    //for (int i = 1; i <= intnum; i++)
                    //{
                    //    if (shape.STInteriorRingN(i) != null && !shape.STInteriorRingN(i).IsNull)
                    //    {
                    //        interiorPoints = shape.STInteriorRingN(i).ss_ToPointsArray<Point>();
                    //        for (int p = 0; p < interiorPoints.Length; p++)
                    //        {
                    //            interiorPoints[p].X = interiorPoints[p].X - tileX * 256;
                    //            interiorPoints[p].Y = interiorPoints[p].Y - tileY * 256;
                    //        }
                    //        _fillTransparentPolygonOnTile(graphics, interiorPoints);
                    //    }
                    //}
                    break;
                case OpenGisGeometryType.MultiPolygon:
                    int subGeomNum = shape.STNumGeometries().Value;

                    for (int g = 1; g <= subGeomNum; g++)
                    {
                        var subGeom = shape.STGeometryN(g);
                        intnum = subGeom.STNumInteriorRing().Value;
                        ring = subGeom.STExteriorRing();

                        using (var gp = new GraphicsPath())
                        {
                            // 1. рисуем полигон без внутренних колец
                            if (ring != null && !ring.IsNull)
                            {
                                exteriorPoints = ring.ss_ToPointsArray<Point>();
                                for (int p = 0; p < exteriorPoints.Length; p++)
                                {
                                    exteriorPoints[p].X = exteriorPoints[p].X - tileX * 256;
                                    exteriorPoints[p].Y = exteriorPoints[p].Y - tileY * 256;
                                }
                                //_fillPolygonOnTile(graphics, fillcolor, exteriorPoints);
                                gp.AddPolygon(exteriorPoints);
                            }
                            // 2. рисуем внутренние кольца
                            for (int i = 1; i <= intnum; i++)
                            {
                                if (subGeom.STInteriorRingN(i) != null && !subGeom.STInteriorRingN(i).IsNull)
                                {
                                    interiorPoints = subGeom.STInteriorRingN(i).ss_ToPointsArray<Point>();
                                    for (int p = 0; p < interiorPoints.Length; p++)
                                    {
                                        interiorPoints[p].X = interiorPoints[p].X - tileX * 256;
                                        interiorPoints[p].Y = interiorPoints[p].Y - tileY * 256;
                                    }
                                    //_fillTransparentPolygonOnTile(graphics, interiorPoints);
                                    gp.AddPolygon(interiorPoints);
                                }
                            }
                            using (Brush brush = new SolidBrush(fillcolor))
                                graphics.FillPath(brush, gp);
                        }

                        //// 1. рисуем полигон без внутренних колец
                        //if (ring != null && !ring.IsNull)
                        //{
                        //    exteriorPoints = ring.ss_ToPointsArray<Point>();
                        //    for (int p = 0; p < exteriorPoints.Length; p++)
                        //    {
                        //        exteriorPoints[p].X = exteriorPoints[p].X - tileX * 256;
                        //        exteriorPoints[p].Y = exteriorPoints[p].Y - tileY * 256;
                        //    }
                        //    _fillPolygonOnTile(graphics, fillcolor, exteriorPoints);
                        //}
                        //// 2. рисуем внутренние кольца
                        //for (int i = 1; i <= intnum; i++)
                        //{
                        //    if (subGeom.STInteriorRingN(i) != null && !subGeom.STInteriorRingN(i).IsNull)
                        //    {
                        //        interiorPoints = subGeom.STInteriorRingN(i).ss_ToPointsArray<Point>();
                        //        for (int p = 0; p < interiorPoints.Length; p++)
                        //        {
                        //            interiorPoints[p].X = interiorPoints[p].X - tileX * 256;
                        //            interiorPoints[p].Y = interiorPoints[p].Y - tileY * 256;
                        //        }
                        //        _fillTransparentPolygonOnTile(graphics, interiorPoints);
                        //    }
                        //}
                    }
                    break;
                case OpenGisGeometryType.GeometryCollection:
                    subGeomNum = shape.STNumGeometries().Value;

                    for (int g = 1; g <= subGeomNum; g++)
                    {
                        _shiftAndDraw(graphics, shape.STGeometryN(g), fillcolor, strokecolor, centercolor, fillWidth, strokeWidth, centerWidth, tileX, tileY);
                    }
                    break;
            }
        }

        static ss_GeometryInstanceInfo<Point> _lonLatToPixels(SqlGeometry geom, int zoom)
        {
            var result = new ss_GeometryInstanceInfo<Point>();
            var pixelsListList = new List<List<ss_GeometryPointSequence<Point>>>();

            result.ShapeType = geom.GetGeometryType();

            List<ss_GeometryPointSequence<PointF>> pointSequenceList;
            var pointsCollection = new List<List<ss_GeometryPointSequence<PointF>>>();
            switch (result.ShapeType)
            {
                case OpenGisGeometryType.Point:
                    PointF[] points = new PointF[1];
                    SqlGeometry firstpoint = geom.STStartPoint();
                    points[0] = new PointF((float)firstpoint.STX, (float)firstpoint.STY);

                    pointSequenceList = new List<ss_GeometryPointSequence<PointF>> { new ss_GeometryPointSequence<PointF> { PointList = points, InnerRing = false } };
                    pointsCollection.Add(pointSequenceList);

                    result.Points = _getGeometryPixelCoords(pointsCollection, zoom);
                    break;
                case OpenGisGeometryType.LineString:
                    points = geom.ss_ToPointsArray<PointF>();
                    pointSequenceList = new List<ss_GeometryPointSequence<PointF>> { new ss_GeometryPointSequence<PointF> { PointList = points, InnerRing = false } };
                    pointsCollection.Add(pointSequenceList);

                    result.Points = _getGeometryPixelCoords(pointsCollection, zoom);
                    break;
                case OpenGisGeometryType.Polygon:
                    pointsCollection = geom.ss_ToGeometryPointsOfPolygon<PointF>();
                    result.Points = _getGeometryPixelCoords(pointsCollection, zoom);
                    break;
                case OpenGisGeometryType.MultiPoint:
                case OpenGisGeometryType.MultiLineString:
                    pointsCollection = geom.ss_ToGeometryPointsOfMultiPointLineString<PointF>();
                    result.Points = _getGeometryPixelCoords(pointsCollection, zoom);
                    break;

                case OpenGisGeometryType.MultiPolygon:
                    pointsCollection = geom.ss_ToGeometryPointsOfMultiPolygon<PointF>();
                    result.Points = _getGeometryPixelCoords(pointsCollection, zoom);
                    break;

                case OpenGisGeometryType.GeometryCollection:
                    var geomNum = geom.STNumGeometries().Value;
                    var geometryList = new ss_GeometryInstanceInfo<Point>[geomNum];
                    for (int i = 1; i <= geomNum; i++)
                    {
                        geometryList[i - 1] = _lonLatToPixels(geom.STGeometryN(i), zoom);
                    }
                    result.GeometryInstanceInfoCollection = geometryList;
                    break;
            }

            return result;
        }
        static SqlGeometry _createGeometry(ss_GeometryInstanceInfo<Point> pixelData)
        {
            Point point;
            int geomnum;

            var builder = new SqlGeometryBuilder();
            builder.SetSrid(0);
            OpenGisGeometryType type = pixelData.ShapeType;
            switch (type)
            {
                case OpenGisGeometryType.Point:
                    builder.BeginGeometry(OpenGisGeometryType.Point);

                    //if (pixelData.Points != null)
                    //{
                    point = pixelData.Points[0][0].PointList[0];
                    builder.BeginFigure(point.X, point.Y);
                    builder.EndFigure();
                    //}
                    builder.EndGeometry();
                    break;
                case OpenGisGeometryType.MultiPoint:
                    builder.BeginGeometry(OpenGisGeometryType.MultiPoint);
                    geomnum = pixelData.Points.Count;
                    for (int i = 0; i < geomnum; i++)
                    {
                        builder.BeginGeometry(OpenGisGeometryType.Point);
                        point = pixelData.Points[i][0].PointList[0];
                        builder.BeginFigure(point.X, point.Y);
                        builder.EndFigure();
                        builder.EndGeometry();
                    }
                    builder.EndGeometry();
                    break;
                case OpenGisGeometryType.LineString:
                    builder.BeginGeometry(OpenGisGeometryType.LineString);
                    //if(pixelData.Points != null)
                    _addFigurePoints(builder, pixelData.Points[0][0].PointList);
                    builder.EndGeometry();
                    break;

                case OpenGisGeometryType.MultiLineString:
                    geomnum = pixelData.Points.Count;
                    builder.BeginGeometry(OpenGisGeometryType.MultiLineString);
                    for (int c = 0; c < geomnum; c++)
                    {
                        builder.BeginGeometry(OpenGisGeometryType.LineString);
                        _addFigurePoints(builder, pixelData.Points[c][0].PointList);
                        builder.EndGeometry();
                    }
                    builder.EndGeometry();
                    break;
                case OpenGisGeometryType.Polygon:

                    builder.BeginGeometry(OpenGisGeometryType.Polygon);
                    _addRings(builder, pixelData.Points[0]);
                    builder.EndGeometry();
                    break;
                case OpenGisGeometryType.MultiPolygon:
                    geomnum = pixelData.Points.Count;
                    builder.BeginGeometry(OpenGisGeometryType.MultiPolygon);
                    for (int i = 0; i < geomnum; i++)
                    {
                        builder.BeginGeometry(OpenGisGeometryType.Polygon);
                        _addRings(builder, pixelData.Points[i]);
                        builder.EndGeometry();
                    }
                    builder.EndGeometry();
                    break;
                case OpenGisGeometryType.GeometryCollection:
                    geomnum = pixelData.GeometryInstanceInfoCollection.Length;

                    SqlGeometry unionGeom = null;

                    for (int i = 0; i < geomnum; i++)
                    {
                        var pi = pixelData.GeometryInstanceInfoCollection[i];

                        if (unionGeom == null)
                            unionGeom = _createGeometry(pi);
                        else
                            unionGeom = unionGeom.STUnion(_createGeometry(pi));
                    }

                    if (unionGeom == null)
                        return SqlGeometry.Null;
                    else
                        return unionGeom;
            }
            SqlGeometry result = builder.ConstructedGeometry;
            if (result.STIsValid()) return result;
            else return result.MakeValid();
        }

        #region helpers
        static List<List<ss_GeometryPointSequence<Point>>> _getGeometryPixelCoords(List<List<ss_GeometryPointSequence<PointF>>> geomInstanceGeoPoints, int zoom)
        {
            var pixelCoordsListList = new List<List<ss_GeometryPointSequence<Point>>>();
            foreach (var list in geomInstanceGeoPoints)
            {
                var geomPixCoordsList = new List<ss_GeometryPointSequence<Point>>();
                foreach (var pointseq in list)
                {
                    var coords = new ss_GeometryPointSequence<Point>();
                    coords.PointList = _getPixelCoords(pointseq.PointList, zoom);
                    coords.InnerRing = pointseq.InnerRing;
                    geomPixCoordsList.Add(coords);
                }
                pixelCoordsListList.Add(geomPixCoordsList);
            }
            return pixelCoordsListList;
        }
        static Point[] _getPixelCoords(PointF[] geopoints, int zoom)
        {
            var pixelpoints = new Point[geopoints.Length];
            for (int i = 0; i < geopoints.Length; i++)
                pixelpoints[i] = new Point
                {
                    X = Helpers.FromLongitudeToXPixel(geopoints[i].X, zoom),
                    Y = Helpers.FromLatitudeToYPixel(geopoints[i].Y, zoom)
                };

            return pixelpoints;
        }
        static void _addRings(SqlGeometryBuilder builder, List<ss_GeometryPointSequence<Point>> ring)
        {
            var circnum = ring.Count;

            for (int c = 0; c < circnum; c++)
                _addFigureRingPoints(builder, ring[c].PointList);
        }
        static void _addFigurePoints(SqlGeometryBuilder builder, Point[] points)
        {
            int geomlen = points.Length;
            int unicqty = points.Distinct().Count();
            if (unicqty < 2) return;

            Point point = points[0];
            Point prevPoint = point;

            builder.BeginFigure(point.X, point.Y);
            for (int i = 1; i < geomlen; i++)
            {
                point = points[i];
                if (point != prevPoint) builder.AddLine(point.X, point.Y);
                prevPoint = point;
            }
            builder.EndFigure();
        }
        static void _addFigureRingPoints(SqlGeometryBuilder builder, Point[] points)
        {
            //try
            //{
                int pointqty = points.Length;
                int unicqty = points.Distinct().Count();
                if (unicqty < 3) return;

                if (points[0] != points[pointqty - 1])
                    return;

                Point point = points[0];
                Point prevPoint = point;

                builder.BeginFigure(point.X, point.Y);
                for (int i = 1; i < pointqty; i++)
                {
                    point = points[i];
                    if (point != prevPoint) builder.AddLine(point.X, point.Y);
                    prevPoint = point;
                }
                builder.EndFigure();
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
        }

        static SqlGeometry _polygonToMultiLineString(SqlGeometry poly)
        {
            SqlGeometry result;
            int intnum = (int)poly.STNumInteriorRing();
            var exteriorRing = poly.STExteriorRing();
            var geomBuilder = new SqlGeometryBuilder();
            geomBuilder.SetSrid((0));

            _addMultiLineStringFromPolygon(geomBuilder, poly);

            result = geomBuilder.ConstructedGeometry;
            if (result.STIsValid()) return result;
            else return result.MakeValid();
        }
        static void _addMultiLineStringFromPolygon(SqlGeometryBuilder geomBuilder, SqlGeometry poly)
        {

            int intnum = poly.STNumInteriorRing().Value;
            var exteriorRing = poly.STExteriorRing();

            if (intnum > 0)
                geomBuilder.BeginGeometry(OpenGisGeometryType.MultiLineString);

            geomBuilder.BeginGeometry(OpenGisGeometryType.LineString);

            var startpoint = exteriorRing.STStartPoint();

            geomBuilder.BeginFigure((double)startpoint.STX, (double)startpoint.STY);
            for (int i = 2; i <= exteriorRing.STNumPoints(); i++)
            {
                geomBuilder.AddLine((double)exteriorRing.STPointN(i).STX, (double)exteriorRing.STPointN(i).STY);
            }

            geomBuilder.EndFigure();
            geomBuilder.EndGeometry();

            if (intnum > 0)
            {
                SqlGeometry intRing;
                SqlGeometry point;
                for (int i = 1; i <= intnum; i++)
                {
                    intRing = poly.STInteriorRingN(i);
                    geomBuilder.BeginGeometry(OpenGisGeometryType.LineString);
                    startpoint = intRing.STStartPoint();
                    geomBuilder.BeginFigure((double)startpoint.STX, (double)startpoint.STY);
                    for (int p = 2; p <= intRing.STNumPoints(); p++)
                    {
                        point = intRing.STPointN(p);
                        geomBuilder.AddLine((double)point.STX, (double)point.STY);
                    }
                    geomBuilder.EndFigure();
                    geomBuilder.EndGeometry();
                }
                geomBuilder.EndGeometry();
            }
        }

        static void _fillPolygonOnTile(Graphics graphics, Color color, Point[] points)
        {
            using (Brush brush = new SolidBrush(color))
                graphics.FillPolygon(brush, points);
        }
        static void _fillTransparentPolygonOnTile(Graphics graphics, Point[] points)
        {
            graphics.FillPolygon(Brushes.Transparent, points);
        }

        //Aux Drawing
        static void _drawContourOnTile(Graphics graphics, SqlGeometry stroke, Color strokecolor, int width, int tileX, int tileY)
        {
            switch (stroke.GetGeometryType())
            {
                case OpenGisGeometryType.MultiLineString:
                    _drawMultiLineString(graphics, stroke, strokecolor, width, tileX, tileY);
                    break;
                case OpenGisGeometryType.LineString:
                    var shiftedPoints = stroke.ss_ToPointsArray<Point>();
                    for (int p = 0; p < shiftedPoints.Length; p++)
                    {
                        shiftedPoints[p].X = shiftedPoints[p].X - tileX * 256;
                        shiftedPoints[p].Y = shiftedPoints[p].Y - tileY * 256;
                    }
                    _drawStroke(graphics, strokecolor, width, shiftedPoints);
                    break;
            }
        }
        static void _drawMultiLineString(Graphics graphics, SqlGeometry stroke, Color strokecolor, int width, int tileX, int tileY)
        {
            using (Pen pen = new Pen(strokecolor, width))
            {
                int geomnum = stroke.STNumGeometries().Value;

                for (int i = 1; i <= geomnum; i++)
                {
                    var subGeom = stroke.STGeometryN(i);
                    if (subGeom != null && !subGeom.IsNull)
                    {
                        var shiftedPoints = subGeom.ss_ToPointsArray<Point>();
                        for (int p = 0; p < shiftedPoints.Length; p++)
                        {
                            shiftedPoints[p].X = shiftedPoints[p].X - tileX * 256;
                            shiftedPoints[p].Y = shiftedPoints[p].Y - tileY * 256;
                        }
                        graphics.DrawLines(pen, shiftedPoints);
                    }
                }
            }
        }
        static void _drawMultiLineStringBordered(Graphics graphics, 
            SqlGeometry stroke, 
            Color strokecolor, 
            Color bordercolor, 
            Color centercolor, 
            int fillWidth, 
            int strokeWidth, 
            int centerWidth,
            int tileX, 
            int tileY)
        {
            int geomnum = (int)stroke.STNumGeometries();
            using (var pen = new Pen(Color.White))
            {
                for (int i = 1; i <= geomnum; i++)
                {
                    var subGeom = stroke.STGeometryN(i);
                    if (subGeom != null && !subGeom.IsNull)
                    {
                        var shiftedPoints = subGeom.ss_ToPointsArray<Point>();
                        for (int p = 0; p < shiftedPoints.Length; p++)
                        {
                            shiftedPoints[p].X = shiftedPoints[p].X - tileX * 256;
                            shiftedPoints[p].Y = shiftedPoints[p].Y - tileY * 256;
                        }
                        _drawBorderedStroke(graphics, pen, strokecolor, bordercolor, centercolor, fillWidth, strokeWidth, centerWidth, shiftedPoints);
                    }
                }
            }
        }

        // Drawing Stroke
        static void _drawStroke(Graphics graphics, Color color, int width, Point[] points)
        {
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            using (var pen = new Pen(color, width))
            {
                graphics.DrawLines(pen, points);
            }
        }
        static void _drawBorderedStroke(Graphics graphics, 
            Pen pen, 
            Color fillColor, 
            Color strokeColor, 
            Color centerColor, 
            int fillWidth, 
            int strokeWidth, 
            int centerWidth,
            Point[] points)
        {
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            _drawStrokeByPen(graphics, pen, strokeColor, strokeWidth * 2 + fillWidth, points);
            _drawStrokeByPen(graphics, pen, fillColor, fillWidth, points);
            _drawStrokeByPen(graphics, pen, centerColor, centerWidth, points);
        }
        static void _drawStrokeByPen(Graphics graphics, Pen pen, Color color, int width, Point[] points)
        {
            pen.Color = color;
            pen.Width = width;
            graphics.DrawLines(pen, points);
        }

        //получаем геометрию тайла, который на 1 пиксел с каждой стороны больше заданного
        static SqlGeometry _getTilePixelBound(int leftX, int rightX, int topY, int bottomY, int Ext)
        {
            leftX = leftX - Ext;
            rightX = rightX + Ext;
            topY = topY - Ext;
            bottomY = bottomY + Ext;

            var geomBuilder = new SqlGeometryBuilder();
            geomBuilder.SetSrid((0));
            geomBuilder.BeginGeometry(OpenGisGeometryType.Polygon);
            geomBuilder.BeginFigure(leftX * 256, topY * 256);
            geomBuilder.AddLine(rightX * 256, topY * 256);
            geomBuilder.AddLine(rightX * 256, bottomY * 256);
            geomBuilder.AddLine(leftX * 256, bottomY * 256);
            geomBuilder.AddLine(leftX * 256, topY * 256);
            geomBuilder.EndFigure();
            geomBuilder.EndGeometry();
            return geomBuilder.ConstructedGeometry;
        }
        #endregion
    }
}
