﻿using Microsoft.SqlServer.Types;
using System;
using System.Drawing;
using System.Text;

namespace WebGIS.TileRendering.Conversions
{ 
    public class Helpers
    {
        private const double PixelTileSize = 256d;
        private const double DegreesToRadiansRatio = 180d / Math.PI;
        private const double RadiansToDegreesRatio = Math.PI / 180d;

        //From coords to pixel
        public static int FromLongitudeToXPixel(double Longitude, double zoomLevel)
        {
            var pixelGlobeSize = PixelTileSize * Math.Pow(2.0, (double)zoomLevel);
            int x = (int)Math.Round((double)(Convert.ToSingle(pixelGlobeSize / 2d) + (Longitude * (pixelGlobeSize / 360d))));
            return x;
        }
        public static int FromLatitudeToYPixel(double Latitude, double zoomLevel)
        {
            var pixelGlobeSize = PixelTileSize * Math.Pow(2.0, (double)zoomLevel);
            var f = Math.Min(Math.Max(Math.Sin((double)(Latitude * RadiansToDegreesRatio)), -0.9999d), 0.9999d);
            int y = (int)Math.Round(Convert.ToSingle(pixelGlobeSize / 2d) + .5d * Math.Log((1d + f) / (1d - f)) * -(pixelGlobeSize / (2d * Math.PI)));
            return y;
        }

        public static Point GetPointFromCoords(double Longitude, double Latitude, double zoomLevel)
        {
            var pixelGlobeSize = PixelTileSize * Math.Pow(2.0, (double)zoomLevel);
            int x = (int)Math.Round((double)(Convert.ToSingle(pixelGlobeSize / 2d) + (Longitude * (pixelGlobeSize / 360d))));
            var f = Math.Min(Math.Max(Math.Sin((double)(Latitude * RadiansToDegreesRatio)), -0.9999d), 0.9999d);
            int y = (int)Math.Round(Convert.ToSingle(pixelGlobeSize / 2d) + .5d * Math.Log((1d + f) / (1d - f)) * -(pixelGlobeSize / (2d * Math.PI)));
            return new Point(x, y);
        }
        public static PointF GetPointFromPixelPosition(long pixelX, long pixelY, int zoomLevel)
        {
            var pixelGlobeSize = PixelTileSize * Math.Pow(2d, (double)zoomLevel);
            var XPixelsToDegreesRatio = pixelGlobeSize / 360d;
            double halfPixelGlobeSize = Convert.ToDouble(pixelGlobeSize / 2d);
            var longitude = (pixelX - halfPixelGlobeSize) / XPixelsToDegreesRatio;
            double YPixelsToRadiansRatio = pixelGlobeSize / (2d * Math.PI);
            var latitude = (2 * Math.Atan(Math.Exp(((double)pixelY - halfPixelGlobeSize) / -YPixelsToRadiansRatio)) - Math.PI / 2) * DegreesToRadiansRatio;
            return new PointF(Convert.ToSingle(longitude), Convert.ToSingle(latitude));
        }

        //From coords to pixel
        public static double FromXPixelToLon(long pixelX, double zoomLevel)
        {
            var pixelGlobeSize = PixelTileSize * Math.Pow(2d, (double)zoomLevel);
            var XPixelsToDegreesRatio = pixelGlobeSize / 360d;
            double halfPixelGlobeSize = Convert.ToSingle(pixelGlobeSize / 2d);
            var longitude = (pixelX - halfPixelGlobeSize) / XPixelsToDegreesRatio;
            return Convert.ToDouble(longitude);
        }
        public static double FromYPixelToLat(long pixelY, double zoomLevel)
        {
            var pixelGlobeSize = PixelTileSize * Math.Pow(2d, zoomLevel);
            double YPixelsToRadiansRatio = pixelGlobeSize / (2d * Math.PI);
            double halfPixelGlobeSize = Convert.ToDouble(pixelGlobeSize / 2d);
            var latitude = (2 * Math.Atan(Math.Exp(((double)pixelY - halfPixelGlobeSize) / -YPixelsToRadiansRatio)) - Math.PI / 2) * DegreesToRadiansRatio;
            return Convert.ToDouble(latitude);
        }

        public static SqlGeometry GetGeoRectangle(double leftX, double topY, double rightX, double bottomY)
        {
            var geomBuilder = new SqlGeometryBuilder();
            geomBuilder.SetSrid((0));
            geomBuilder.BeginGeometry(OpenGisGeometryType.Polygon);
            geomBuilder.BeginFigure(leftX, topY);
            geomBuilder.AddLine(leftX, bottomY);
            geomBuilder.AddLine(rightX, bottomY);
            geomBuilder.AddLine(rightX, topY);
            geomBuilder.AddLine(leftX, topY);
            geomBuilder.EndFigure();
            geomBuilder.EndGeometry();
            return geomBuilder.ConstructedGeometry;
        }
        public static SqlGeometry GetGeoRectangle(PointF topleft, PointF bottomright)
        {
            var geomBuilder = new SqlGeometryBuilder();
            geomBuilder.SetSrid((0));
            geomBuilder.BeginGeometry(OpenGisGeometryType.Polygon);
            geomBuilder.BeginFigure(topleft.X, topleft.Y);
            geomBuilder.AddLine(topleft.X, bottomright.Y);
            geomBuilder.AddLine(bottomright.X, bottomright.Y);
            geomBuilder.AddLine(bottomright.X, topleft.Y);
            geomBuilder.AddLine(topleft.X, topleft.Y);
            geomBuilder.EndFigure();
            geomBuilder.EndGeometry();
            return geomBuilder.ConstructedGeometry;
        }

        //получаем долготу тайла tileX
        public static double getLongitudeFromTilePos(int XTilePos, int Zoom)
        {
            return (XTilePos / Math.Pow(2.0, Zoom) * 360.0 - 180.0);
        }
        //получаем широту тайла tileY
        public static double getLatitudeFromTilePos(int YTilePos, int Zoom)
        {
            var n = Math.PI - 2.0 * Math.PI * YTilePos / Math.Pow(2.0, Zoom);
            return (180.0 / Math.PI * Math.Atan(0.5 * (Math.Exp(n) - Math.Exp(-n))));
        }
        //получаем координату (X в пикселях) которому соответствует точка с долготой = lon при масштабе = zoom
        public static int getPixelXPosFromLongitude(double Longitude, int zoomLevel)
        {
            var pixelGlobeSize = PixelTileSize * Math.Pow(2.0, (double)zoomLevel);
            int x = (int)Math.Round((double)(Convert.ToSingle(pixelGlobeSize / 2d) + (Longitude * (pixelGlobeSize / 360d))));
            return x;
        }
        //получаем координату (Y в пикселях) которому соответствует точка с широтой = lat при масштабе = zoom
        public static int getPixelYPosFromLatitude(double Latitude, int zoomLevel)
        {
            var pixelGlobeSize = PixelTileSize * Math.Pow(2.0, (double)zoomLevel);
            var f = Math.Min(Math.Max(Math.Sin((double)(Latitude * RadiansToDegreesRatio)), -0.9999d), 0.9999d);
            int y = (int)Math.Round(Convert.ToSingle(pixelGlobeSize / 2d) + .5d * Math.Log((1d + f) / (1d - f)) * -(pixelGlobeSize / (2d * Math.PI)));
            return y;
        }
        //получаем долготу по координате (X в пикселях) при масштабе = zoom
        public static double getLatitudeFromPixelYPos(double pixelY, int Zoom)
        {
            double DegreesToRadiansRatio = 180 / Math.PI;
            double PixelTileSize = 256;
            double pixelGlobeSize = PixelTileSize * Math.Pow(2, Zoom);
            double halfPixelGlobeSize = pixelGlobeSize / 2;
            double YPixelsToRadiansRatio = pixelGlobeSize / (2 * Math.PI);

            return (2 * Math.Atan(Math.Exp((pixelY - halfPixelGlobeSize) / -YPixelsToRadiansRatio)) - Math.PI / 2) * DegreesToRadiansRatio;
        }
        //получаем широту по координате (Y в пикселях) при масштабе = zoom
        public static double getLongitudeFromPixelXPos(double pixelX, int Zoom)
        {
            int PixelTileSize = 256;
            double pixelGlobeSize, XPixelsToDegreesRatio;

            pixelGlobeSize = PixelTileSize * Math.Pow(2, Zoom);
            XPixelsToDegreesRatio = pixelGlobeSize / 360;

            double halfPixelGlobeSize = pixelGlobeSize / 2;
            return (pixelX - halfPixelGlobeSize) / XPixelsToDegreesRatio;
        }
        //получаем номер тайла (tileX) в который попадает точка с долготой = lon
        public static int getXTilePos(double Longitude, int zoom)
        {
            var D = 128 * Math.Pow(2, zoom);
            var E = Math.Round(D + Longitude * 256 / 360 * Math.Pow(2, zoom), 0);
            int tileX = (int)Math.Floor(E / 256);

            return tileX;
        }
        //получаем номер тайла (tileY) в который попадает точка с широтой = lat
        public static int getYTilePos(double Latitude, int zoom)
        {
            var D = 128 * Math.Pow(2, zoom);
            var A = Math.Sin(Math.PI / 180 * Latitude);
            var B = -0.9999;
            var C = 0.9999;

            if (A < B) A = B;
            if (A > C) A = C;
            var F = A;

            var G = Math.Round(D + 0.5 * Math.Log((1.0 + F) / (1.0 - F)) * (-256) * Math.Pow(2, zoom) / (2 * Math.PI), 0);
            int tileY = (int)Math.Floor(G / 256);

            return tileY;
        }

        public static void QuadKeyToTileXY(string quadKey, out int tileX, out int tileY, out int levelOfDetail)
        {
            tileX = tileY = 0;
            levelOfDetail = quadKey.Length;
            for (int i = levelOfDetail; i > 0; i--)
            {
                int mask = 1 << (i - 1);
                switch (quadKey[levelOfDetail - i])
                {
                    case '0':
                        break;

                    case '1':
                        tileX |= mask;
                        break;

                    case '2':
                        tileY |= mask;
                        break;

                    case '3':
                        tileX |= mask;
                        tileY |= mask;
                        break;

                    default:
                        throw new ArgumentException("Invalid QuadKey digit sequence.");
                }
            }
        }
        public static string TileXYToQuadKey(int tileX, int tileY, int levelOfDetail)
        {
            StringBuilder quadKey = new StringBuilder();
            for (int i = levelOfDetail; i > 0; i--)
            {
                char digit = '0';
                int mask = 1 << (i - 1);
                if ((tileX & mask) != 0)
                {
                    digit++;
                }
                if ((tileY & mask) != 0)
                {
                    digit++;
                    digit++;
                }
                quadKey.Append(digit);
            }
            return quadKey.ToString();
        }
        public static SqlGeometry GetTileBound(int tileX, int tileY, int Zoom, int Ext)
        {
            double top, left, right, bottom;

            left = WebGIS.TileRendering.Conversions.Helpers.getLongitudeFromTilePos(tileX, Zoom);
            right = WebGIS.TileRendering.Conversions.Helpers.getLongitudeFromTilePos(tileX + 1, Zoom);

            top = WebGIS.TileRendering.Conversions.Helpers.getLatitudeFromTilePos(tileY, Zoom);
            bottom = WebGIS.TileRendering.Conversions.Helpers.getLatitudeFromTilePos(tileY + 1, Zoom);

            return WebGIS.TileRendering.Conversions.Helpers.GetGeoRectangle(left, top, right, bottom);
        }
    }
}
